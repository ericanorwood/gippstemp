<?php

class CustomCSSandJS_LicenseForm {

    var $data = array();

    function __construct( $data = array() ) {
        $data['nonce'] = $data['license'] . '_nonce';
        $data['activate'] = $data['license'] . '_activate';
        $data['deactivate'] = $data['license'] . '_deactivate';
        $data['license_domain'] = $data['license'] . '_domain';
        $this->data = $data;


        if( !class_exists( 'EDD_SL_Plugin_Updater_CCS' ) ) {
            include( dirname( __FILE__ ) . '/EDD_SL_Plugin_Updater.php' );
        }
    }


    /**
     * License form 
     */
    function license_page( $admin_notice ) {
        $license 	= get_option( $this->data['license_key'] );
        $domain     = get_option( $this->data['license_domain'] );
        $status 	= get_option( $this->data['license_status'] );

        $license_data = false;
        if( $status !== false && $status == 'valid' ) {
            $api_params = array(
                'edd_action'=> 'check_license',
                'license' => $license, 
                'item_name' => urlencode( $this->data['item_name']),
                'url'       => home_url()
            );

            $response = wp_remote_post( $this->data['store_url'], array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

            if ( ! is_wp_error( $response ) ) {
                $license_data = json_decode( wp_remote_retrieve_body( $response ) );
            }

            if ( !$domain && isset($license_data->license) && $license_data->license == 'valid' ) {
                $home_url = str_replace( array('https://', 'http://'), '', home_url() );
                $domain = $home_url;
                update_option( $this->data['license_domain'], $home_url );
            }
        }

        ?>
        <?php if ( isset($admin_notice['msg']) && !empty($admin_notice['msg']) ) : ?>
            <div id="alert_messages">
                <div class="alert alert-dismissable alert-<?php echo $admin_notice['class']; ?>">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <?php echo $admin_notice['msg']; ?>
                </div>
            </div>
        <?php endif; ?>

        <script type="text/javascript">
            jQuery(document).ready(function( $ ){
                var license_input = $('#<?php echo $this->data['license_key']; ?>');

                /* Enable/disable the Activate/Deactivate buttons */
                if (license_input.val().length > 0 ) {
                    $('#license-deactivate').removeAttr('disabled');
                }

                var license_input_disable = function() {
                    if ( license_input.val().length > 0 ) {
                        $('#license-activate').removeAttr('disabled');
                    } else {
                        $('#license-activate').attr('disabled', 'disabled');
                        $('#license-deactivate').attr('disabled', 'disabled');
                    }
                }
                license_input_disable();
                license_input.on('input', function() {
                    license_input_disable();
                });

            });
        </script>

        <style type="text/css">
            button:disabled {
                cursor: not-allowed !important;
            }
            .panel-body a {
                text-decoration: underline;
            }
        </style>

            <form method="post" class="form-inline">

                <?php settings_fields( $this->data['license'] ); ?>

                <div class="form-group" style="margin-top: 20px; margin-bottom: 30px;">
                    <input id="<?php echo $this->data['license_key']; ?>" name="<?php echo $this->data['license_key']; ?>" type="text" class="regular-text" value="<?php esc_attr_e( $license ); ?>" placeholder="Enter you license key" />
                    <button id="license-activate" class="button-secondary" name="<?php echo $this->data['activate']; ?>" value="Activate License" />Activate License</button>
                    <button id="license-deactivate" disabled class="button-secondary" name="<?php echo $this->data['deactivate']; ?>" value="Deactivate License" />Deactivate License</button>
                    <?php wp_nonce_field( $this->data['nonce'], $this->data['nonce'] ); ?>
                </div>

        <?php

        $messages = array();

        if( $status !== false && $status == 'valid' ) {
            $renewal_link = 'https://www.silkypress.com/checkout/?edd_license_key=' . urlencode( $license ) . '&amp;utm_campaign=admin&amp;utm_source=licenses&amp;utm_medium=renew';
            $this_domain = str_replace(array('https://', 'http://'), '', home_url()); 
            $expires       = '';
            if ( isset($license_data->expires) && $license_data->expires != 'lifetime' ) {
                try {
                    $date_format = new DateTime( $license_data->expires );
                    $expires = date_format( $date_format, 'j M, Y');
                } catch (Exception $e) {
                    $expires = '';
                }
            }
            if ( !isset($license_data->license) ) $license_data->license = '';


            // Message: error, the license wasn't activated for this domain
            if ( $domain && !empty($domain) && $domain != $this_domain ) {
                $messages[] = array(
                    'type' => 'danger',
                    'message' => sprintf( __('This license was activated for the <b>%s</b> website, not <b>%s</b> website. This will lead to an "Unauthorized" error when attempting to update the plugin. If you click the "Deactivate License" and then the "Activate License" button, then the update will work alright.'), $domain, $this_domain ),
                );
            }


            // Message: error, the license isn't active for this domain 
            if ( $license_data->license == 'site_inactive' ) {
                $messages[] = array(
                    'type' => 'danger',
                    'message' => sprintf( __('This license was activated in the past, but the domain changed. Currently the license isn\'t activated for the <b>%s</b> domain. This will lead to an "Unauthorized" error when attempting to update the plugin. If you click the "Deactivate License" and then the "Activate License" button, then the update will work alright.'), $this_domain ),
                );
            }


            // Message: error, the license is expired 
            if ( $license_data->license == 'expired' ) {
                $messages[] = array(
                    'type' => 'danger',
                    'message' => sprintf( __('This license expired on %s. Please <a href="%s" target="_blank">click here</a> to renew your license so you can keep the plugin up-to-date.'), $expires, $renewal_link),
                );
            }


            // Message: error, the license is inactive 
            if ( $license_data->license == 'inactive' ) {
                $messages[] = array(
                    'type' => 'danger',
                    'message' => sprintf( __('This license was disabled, therefore it cannot be activated.') ),
                );
            }


            // Message: The license is activated on this domain
            if ( $license_data->license == 'valid' ) {
                $messages[] = array(
                    'type' => 'info',
                    'message' => sprintf( __('The <code>%s</code> license is activated on the <code>%s</code> domain name.'), $license, $domain),
                );
            }


            // Message: license active on this domain. Expires on __. Link to customer area
            if ( $license_data && ($license_data->license == 'valid' || $license_data->license == 'expired' ) ) {
                $_limit         = isset($license_data->license_limit) ? $license_data->license_limit : 1;
                if ( $_limit < 2 ) $_limit .= '-site license'; else $_limit .= '-sites license';
                $_site_count    = isset($license_data->site_count) ? $license_data->site_count: 1;
                if ( !empty($expires) ) $expires = sprintf( __('It expires on %s. '), $expires );
                $_link          = 'https://www.silkypress.com/login/';

                if ( $_site_count > 1 ) $_site_count .= ' websites'; else $_site_count = ' one website';

                $messages[] = array(
                    'type' => 'info',
                    'message' => sprintf( __('Your %s is currently active on %s. %sYou can use the <a href="%s" target="_blank">silkypress.com customer area</a> to manage you license.'), $_limit, $_site_count, $expires, $_link),
                );
            }


            // Message: Click here to renew your license. Shown two months before expiring.
            if ( $license_data && ($license_data->license == 'valid' || $license_data->license == 'expired' ) && strtotime( $license_data->expires ) ) {
                if ( strtotime( $license_data->expires ) - time() < 60 * 60 * 24 * 60 ) {
                    $messages[] = array(
                        'type' => 'warning',
                        'message' => sprintf( __('<a href="%s" target="_blank">Click here</a> if you want to renew your license'), $renewal_link),
                    );
                }
            }

            
        } else {


            // Message: error, license inactive
            $messages[] = array(
                'type' => 'danger',
                'message' => sprintf( __('Currently there is no license activated on this website.')),
            );
            
        } 

        if ( count($messages) > 0 ) {
            foreach($messages as $_m) {
                echo '<div class="alert alert-'.$_m['type'].'">' . $_m['message'] . '</div>';
            }
        }

        ?>
        </form>
        <?php
    }

    function register_option() {
        register_setting($this->data['license'], $this->data['license_key'], array( $this, 'edd_sanitize_license') );
    }

    function edd_sanitize_license( $new ) {
        $old = get_option( $this->data['license_key'] );
        if( $old && $old != $new ) {
            delete_option( $this->data['license_status'] ); // new license has been entered, so must reactivate
        }
        return $new;
    }

    /**
     * Activate/deactivate the license
     */
    function activate_deactivate_license() {

        update_option( $this->data['license_key'], $_POST[$this->data['license_key']] ); 

        if( ! isset( $_POST[$this->data['activate']] ) && ! isset( $_POST[$this->data['deactivate']] ) ) {
            return;
        }


        $edd_action = 'activate_license';
        if ( isset( $_POST[$this->data['deactivate']] ) ) {
            $edd_action = 'deactivate_license';
        }


        if( ! check_admin_referer( $this->data['nonce'], $this->data['nonce'] ) )
            return; 


        $api_params = array(
            'edd_action'=> $edd_action,
            'license' 	=> trim( $_POST[$this->data['license_key']] ),
            'item_name' => urlencode( $this->data['item_name']), 
            'url'       => home_url()
        );

        $license_data = wp_remote_post( $this->data['store_url'], array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

        $admin_notice = false;
        if ( is_wp_error( $license_data ) ) {

            $admin_notice = array(
                'class' => 'danger',
                'msg'   => 'Something went wrong during license update request. [' . $license_data->get_error_message() . ']'
            );

            return $admin_notice;

        } else {

            $license_data = json_decode( wp_remote_retrieve_body( $license_data) );

            if ( $license_data->success ) {

                switch ( $license_data->license ) {
                    case 'valid':
                        $admin_notice = array(
                            'class' => 'success',
                            'msg'   => 'Your license is working fine. Good job!'
                        );
                        break;

                    case 'deactivated':
                        $admin_notice = array(
                            'class' => 'success',
                            'msg'   => 'Your license was successfully deactivated for this site.'
                        );
                        break;
                }

                $license_expires = strtotime( $license_data->expires );

            } elseif ( !isset($license_data->error ) ) {

                return false;

            } else {

                switch ( $license_data->error ) {
                    case 'invalid':                 // key do not exist
                    case 'missing':
                    case 'key_mismatch':
                        $admin_notice = array(
                            'class' => 'danger',
                            'msg'   => "Wrong license key. Make sure you're using the correct license."
                        );
                        break;

                    case 'license_not_activable':   // trying to activate bundle license
                        $admin_notice = array(
                            'class' => 'danger',
                            'msg'   => 'If you have a bundle package, please use each individual license for your products.'
                        );
                        break;

                    case 'revoked':                 // license key revoked
                        $admin_notice = array(
                            'class' => 'danger',
                            'msg'   => 'This license was revoked.'
                        );
                        break;

                    case 'no_activations_left':     // no activations left
                        $login_url = 'https://www.silkypress.com/login/';
                        $admin_notice = array(
                            'class' => 'danger',
                            'msg'   => 'No activations left. <a href="'.$login_url.'">Log in to your account</a> to extent your license.'
                        );
                        break;

                    case 'invalid_item_id':
                        $admin_notice = array(
                            'class' => 'danger',
                            'msg'   => 'Invalid item ID.'
                        );
                        break;

                    case 'item_name_mismatch':      // item names don't match
                        $admin_notice = array(
                            'class' => 'danger',
                            'msg'   => "Item names don't match."
                        );
                        break;

                    case 'expired':                 // license has expired
                        $license_key = trim( $_POST[$this->data['license_key']] );
                        $admin_notice = array(
                            'class' => 'danger',
                            'msg'   => 'Your License has expired. <a href="https://www.silkypress.com/checkout/?edd_license_key=' . urlencode( $license_key ) . '&utm_campaign=admin&utm_source=licenses&utm_medium=renew" target="_blank">Renew it now.</a>'
                        );
                        break;

                    case 'inactive':                // license is not active
                        $admin_notice = array(
                            'class' => 'danger',
                            'msg'   => 'This license is not active. Activate it now.'
                        );
                        break;

                    case 'disabled':                // license key disabled
                        $admin_notice = array(
                            'class' => 'danger',
                            'msg'   => 'License key disabled.'
                        );
                        break;

                    case 'site_inactive':
                        $admin_notice = array(
                            'class' => 'danger',
                            'msg'   => 'The license is not active for this site. Activate it now.'
                        );
                        break;

                }

                // add error code
                $admin_notice['msg'] .= " [error: $license_data->error]";

            }

        }


        if ( $edd_action == 'activate_license' ) {
            update_option( $this->data['license_status'], $license_data->license );
            update_option( $this->data['license_domain'], str_replace(array('https://', 'http://'), '', home_url()) );
        } elseif ( $license_data->license == 'deactivated' ) {
            delete_option( $this->data['license_status'] );
        }

        return $admin_notice;

    }

}
