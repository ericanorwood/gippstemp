��          T      �       �   ?   �      �   :   �   �   :  x   �  4   M  Z  �  }   �     [  h   h  �   �  �   �  d   d                                        Could not activate the %s Extension; GravityView is not active. Error The %s Extension requires GravityView Version %s or newer. There is a new version of %1$s available. <a target="_blank" class="thickbox" href="%2$s">View version %3$s details</a> or <a href="%4$s">update now</a>. There is a new version of %1$s available. <a target="_blank" class="thickbox" href="%2$s">View version %3$s details</a>. You do not have permission to install plugin updates Project-Id-Version: GravityView Featured Entries
POT-Creation-Date: 2015-03-05 23:32-0700
PO-Revision-Date: 2017-08-03 18:14+0000
Last-Translator: Zachary Katz <zack@katz.co>
Language-Team: Russian (http://www.transifex.com/katzwebservices/gravityview-featured-entries/language/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
X-Generator: Poedit 1.7.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SourceCharset: UTF-8
 Не возможно активировать %s дополнение; Плагин GravityView не активирован. Ошибка Дополнению  %s необходим плагинGravityView версии %s или свежее. Новая версия  %1$s доступна. <a target="_blank" class="thickbox" href="%2$s">Узнать подробности о новой версии %3$s здесь</a> или <a href="%4$s">обновить сейчас</a>. Новая версия  %1$s доступна. <a target="_blank" class="thickbox" href="%2$s">Подробности о новой версии %3$s здесь</a>. У вас нет полномочий устанавилвать обновления плагина 