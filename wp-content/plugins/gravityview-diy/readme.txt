=== GravityView DIY ===
Tags: gravityview
Requires at least: 4.4
Tested up to: 4.9.5
Stable tag: trunk
Contributors: The GravityView Team
License: GPL 2 or higher

A flexible, powerful GravityView layout for designers & developers.

== Description ==

DIY is a different kind of View layout: the purpose is to allow designers and developers the most flexibility. DIY allows you to use your own CSS and HTML structure instead of needing to modify our predefined layouts to fit your needs.

== Installation ==

1. Upload plugin files to your plugins folder, or install using WordPress' built-in Add New Plugin installer
2. Activate the plugin
3. Follow the instructions

== Changelog ==

= 2.0.2 on May 8, 2018 =

* Fixed: Error when GravityView is active but Gravity Forms is not

= 2.0 on May 8, 2018 =

* Support for GravityView 2.0
* Added: Turkish translation (thank you, Süha Karalar!)

= 1.1.1 on April 28, 2018 =

* Restored ability to add "Custom Labels" to fields in View Configuration. Labels will not appear on the front end.
* Added filters to prevent DIY Layout from adding any extra HTML
    - `gravityview-diy/wrap/multiple` _bool_ Should the entry in Multiple Entries context be wrapped in minimal HTML containers? Default: true
    - `gravityview-diy/wrap/single` _bool_ Should the entry in Single Entry context be wrapped in minimal HTML containers? Default: true
* Make "Before Content" and "After Content" fields larger, monospaced
* Fixed error when GravityView is not active
* Added translations—[become a translator here](https://www.transifex.com/katzwebservices/gravityview-diy/)
* Preparing for GravityView 2.0

= 1.0 on January 16, 2018 =

* Launch!